/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex1;

import java.util.ArrayList;
import java.util.Scanner;

public class Calculadora {
    
     private static final int PESSOAS = 3;
    private static Scanner input;

    public static void main(String[] args) {
        input = new Scanner(System.in);

        ArrayList<Pessoa> listaDePessoas = new ArrayList<>();
        for(int i = 0; i < PESSOAS; i++){
            Pessoa novaPessoa = new Pessoa();

            System.out.println("Nome: ");
                novaPessoa.setNome(input.nextLine());
            System.out.println("Idade: ");
                novaPessoa.setIdade(input.nextInt());
            input.nextLine();
            listaDePessoas.add(novaPessoa);
        }

        Pessoa maisNova, maisVelha;
        maisNova = maisVelha = listaDePessoas.get(0);

        for(Pessoa pessoa:listaDePessoas){
            if(pessoa.getIdade() > maisVelha.getIdade())
                maisVelha = pessoa;
            else if(pessoa.getIdade() < maisNova.getIdade())
                maisNova = pessoa;
        }

        System.out.printf("Mais nova: %s - %d", maisNova.getNome(), maisNova.getIdade());
        System.out.printf("Mais velha: %s - %d", maisVelha.getNome(), maisVelha.getIdade());         
    }
    
    
    
    
    
    
    
    
    
    
    
    

    
}
